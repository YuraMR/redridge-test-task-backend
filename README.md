# redridge-test-task-backend

## Prerequisites
- [node.js](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/en/)

## Get Started

1. Install dependencies:
    ```bash
    yarn install
    ```

2. Run in development mode:
    ```bash
    yarn start
    ```

3. Production build process:
    ```bash
    yarn build
    ```
