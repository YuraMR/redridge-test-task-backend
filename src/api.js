import express from 'express';
import fs from 'fs';
import log from './logging';
import {createJsonFile} from "./utils/functions";

const router = express.Router();

// middleware
router.use((req, res, next) => {
  const info = {
    time   : new Date(),
    params : req.params,
    url    : req.url,
    method : req.method,
    headers: req.headers,
  };
  // log all the requests to the API
  log.info(`${JSON.stringify(info)}`);
  next();
});

router.get('/', (req, res) => res.send('¡Hola Mundo!'));
router.get('/questions', (req, res) =>
  fs.createReadStream('src/resources/data/questions.json').pipe(res)
);

router.post('/pollTemplates', (req, res) => {
  createJsonFile('src/resources/data/polls', req.body);
  res.send("Success!")
});

export default router;
