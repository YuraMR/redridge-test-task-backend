import fs from 'fs';

const createJsonFile = (path, data) => {
  fs.readdir(path, (err, files) => {
    fs.appendFile(`${path}/poll-${files.length}.json`, JSON.stringify(data), () => console.log("Success!"))
  })
};

export { createJsonFile };
